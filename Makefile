all:main.o packet_handling.o dns_inject.o
	gcc -o dnsinject -lpcap main.o packet_handling.o dns_inject.o
	rm *.o

main.o: main.c packet_handling.h dns_inject.h packet_defns.h
	gcc -c main.c

packet_handling.o:packet_handling.c packet_handling.h packet_defns.h
	gcc -c packet_handling.c

dns_inject.o:dns_inject.c dns_inject.h packet_defns.h
	gcc -c dns_inject.c

clean:
	rm *.o
