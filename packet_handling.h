#include <stdio.h>
#include <pcap/pcap.h>
#include <stdlib.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <ctype.h>
#include "packet_defns.h"

void identify_protocol( u_char ip_p );
void print_payload( int length, u_char *payload);
void print_ether_type( u_short ether_type);
void print_host_addr(const u_char host_addr[ETHER_ADDR_LENGTH] );
void print_src_dest_ip(struct sniff_ip *ip);
void print_src_dest_udp(struct sniff_udp *udp);
void print_src_dest_ip_with_port(struct sniff_ip *ip, struct sniff_tcp *tcp);
void print_ethernet_frame_details(const u_char *packet, const struct pcap_pkthdr *header);
