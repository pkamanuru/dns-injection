#include <stdio.h>
#include <sys/types.h>
#include "packet_defns.h"
#include <pcap/pcap.h>

void swap_bytes(void *addr_1, void *addr_2, int no_of_bytes);
int create_raw_socket();
void print_payload_details( const struct pcap_pkthdr *header, struct sniff_ip *ip,
                            struct sniff_udp *udp, struct sniff_dns *dns);
void print_dns_details( const char *packet );
void prepare_destination_address(struct sniff_ip *ip, struct sniff_udp *udp, struct sockaddr_in *sockaddr);
void write_dns_answer( void *data, struct in_addr *resp_addr, struct dns_question *dns );
uint16_t ip_checksum(const void *buf, size_t hdr_len);
uint16_t udp_checksum(const void *buff, size_t len, in_addr_t src_addr, in_addr_t dest_addr);
